// get all .txt files the the current folder , get stats for same , and the contents of same
// const debug = require('debug')('promisesEach');
const debug = require('debug')('promisesEach:log')
var error = require('debug')('promisesEach:error');

var
async = require('async'),
    fs = require('fs');
const _ = require('lodash');

async.waterfall([
        function(callback) {
            fs.readdir('.', callback); //read the current directory, pass it along to the next function.
        },
        function(fileNames, callback) { //`fileNames` is the directory listing from the previous function
            // console.log(fileNames);

            var filteredList = _.filter(fileNames, (a) => _.endsWith(a, '.txt'));
            // console.log(filteredList);
            async.map(
                filteredList, //The directory listing is just an array of filenames,
                fs.stat, //so we can use async.map to run fs.stat for each filename
                function(err, stats) {
                    if (err) { callback(err); } else {
                        // console.log('stats');
                        // console.dir(stats);
                        callback(err, filteredList, stats); //pass along the error, the directory listing and the stat collection to the next item in the waterfall
                    }
                }
            );
        },
        function(fileNames, stats, callback) { //the directory listing, `fileNames` is joined by the collection of fs.stat objects in  `stats`
            async.map(
                fileNames,
                function(aFileName, readCallback) { //This time we're taking the filenames with map and passing them along to fs.readFile to get the contents
                    fs.readFile(aFileName, 'utf8', readCallback);
                },
                function(err, contents) {
                    if (err) { callback(err); } else { //Now our callback will have three arguments, the original directory listing (`fileNames`), the fs.stats collection and an array of with the contents of each file
                        callback(err, fileNames, stats, contents);
                    }
                }
            );
        }
    ],
    function(err, fileNames, stats, contents) {
        if (err) {
            error('error encountered is %O', err);
        } else {
            // console.log(fileNames);
            debug('FileNames are : %o ', fileNames)
                // console.log(stats);
            debug('stats are : %O ', stats)
                // console.log(contents);
            debug('contents are : %O ', contents)
        }
    }
);