const debug = require('debug')('simpleWaterFall:log')
var error = require('debug')('simpleWaterFall:error');
// console.log('series promises')
const async = require('async');
const Promise = require('bluebird');
const fs = require('fs');

//series
async.waterfall([
        //read a directory 
        function(callback) {
            fs.readdir('.', callback)
        },
        //result of directory result will now be passed to new in list and so on.
        function(args1, callback) {
            debug('Params got from above promise success : ' + args1);
            debug('Getting stats for File : ' + args1[0]);
            fs.stat(args1[0], callback)
        }
    ],
    // optional callback
    function(err, results) {
        // results is now equal to ['one', 'two']
        if (err) {
            error('Error encountered %O', err);
            // console.dir(err)
        } else {
            debug('Final result is %O', results);
            // console.dir(results)
        }

    });