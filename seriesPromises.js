// const debug = require('debug')('seriesPromise')
const debug = require('debug')('seriesPromise:log')
var error = require('debug')('seriesPromise:error');
debug('Starting');
const async = require('async');
const Promise = require('bluebird');
const fs = require('fs');


//series executed one after other
async.series([
        // need to wrap the async function call in a function with a callback 
        // param which needs to be set as a CB when the async function gets over
        function(callback) {
            fs.stat('yarn.lock', callback)
        },
        function(callback) {
            fs.stat('package.json', callback)
        }

        // Shorter Method
        // async.apply(fs.stat, 'yarn.lock'),
        // async.apply(fs.stat, 'package.json')
    ],
    // optional callback
    function(err, results) {
        // results is now equal to ['one', 'two']
        if (err) {
            error('Error encountered is  %O ', err)
        } else {
            debug('result is %O', results)
        }

    });