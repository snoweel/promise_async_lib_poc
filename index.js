const debug = require('debug')('index')
debug('starting project');
require('./mapPromises')
require('./promisifyLibraries')
require('./seriesPromises')
require('./waterFallPromise')
require('./promisesEach');

debug('Completed  execution');

// require('./parallelPromises')