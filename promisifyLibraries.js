const debug = require('debug')('promisifyLibraries:log')
    // var error = require('debug')('simpleWaterFall:error');
    // console.log('promisifyLibrarries');
const async = require('async');
const Promise = require('bluebird');
var fs = Promise.promisifyAll(require("fs"));

const fileNames = ['package.json', 'yarn.lock', 'index.js'];

Promise.map(fileNames, function(fileName) {
    // Promise.map awaits for returned promises as well.
    debug('BlueBird call for filename : ', fileName);
    return fs.statAsync(fileName);
}).then(function(results) {
    // console.log("done");
    debug('results : %O', results)
});