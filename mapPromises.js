var debug = require('debug')('mapPromise:log');
var error = require('debug')('mapPromise:error');
console.log('map promises')
const async = require('async');
const Promise = require('bluebird');
const fs = require('fs');
var fs_async = Promise.promisifyAll(require("fs"));



const fileNames = ['package.json', 'yarn.lock', 'index.js'];
async.map(fileNames, fs.stat, function(err, results) {
    // results is now an array of stats for each file
    if (err) {
        error(`error message is ${err}`)

    } else {
        debug(JSON.stringify(results, null, 2))
    }

});

// Promise.map(fileNames, function(fileName) {
//     // Promise.map awaits for returned promises as well.
//     console.log(`BlueBird call for filename : ${fileName}`);
//     return fs_async.statAsync(fileName);
// }).then(function(results) {
//     // console.log("done");
//     console.log(JSON.stringify(results, null, 2))
// });